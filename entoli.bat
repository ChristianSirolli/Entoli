<!-- :
@SET "LOCATION=%CD%"
@ECHO OFF
@TITLE Think Diff Project
@COLOR 27
@CLS
@ECHO:                                                ___________
@ECHO:                                               ^/           ^\
@ECHO:                                               ^|   THINK   ^|
@ECHO:                                               ^|    DIFF   ^|
@ECHO:                                               ^|  Project  ^|
@ECHO:                                               ^\___________^/
@ECHO.
@ECHO.
@ECHO:Please report issues at the following URL:
@ECHO: https://gitlab.com/think-differently/Entoli/issues
@TIMEOUT /T 02>nul
@GOTO SETTINGS
:SETTINGS
@ECHO OFF
@REM Change the color of the console
@SET "COLORS=0A"
@REM Set to TRUE to have ECHO equal ON or set to FALSE to have ECHO equal OFF when started
@SET "ECHOTOGGLE=TRUE"
@REM Replace %USERPROFILE% with whatever location you would like to start with. %USERPROFILE% is equal to "C:\Users\<your username>"
@SET "CDDIR=%USERPROFILE%"
@GOTO START
:START
@ECHO OFF
@SETLOCAL EnableDelayedExpansion
@TITLE ^>^>^<^>^> Entoli
@CD %CDDIR%
@COLOR %COLORS%
@CLS
@ECHO:Entoli [Version 1.1.1.2]
@ECHO:Copyright (c) 2019 Christian Sirolli
:CMD
@ECHO OFF
@CD %CDDIR%
@IF /I "%ECHOTOGGLE%"=="true" (
	@SET "ECHO=on"
	@ECHO.
	@SET "CMD="
	@SET /P "CMD=%CDDIR%>"
)
@IF /I "%ECHOTOGGLE%"=="false" (
	@SET "ECHO=off"
	@SET "CMD="
	@SET /P CMD=""
)
@IF /I "%CMD%"=="" (GOTO CMD)
@IF /I "%CMD%"=="@ECHO OFF" (SET "ECHOTOGGLE=false" && GOTO CMD)
@IF /I "%CMD%"=="ECHO OFF" (SET "ECHOTOGGLE=false" && GOTO CMD)
@IF /I "%CMD%"=="@ECHO ON" (SET "ECHOTOGGLE=true" && GOTO CMD)
@IF /I "%CMD%"=="ECHO ON" (SET "ECHOTOGGLE=true" && GOTO CMD)
@IF /I "%CMD%"=="exit" (GOTO EXIT)
@IF /I "%CMD%"=="echo" (ECHO:ECHO is %ECHO%. && GOTO CONTINUE)
@IF /I "%CMD%"=="debug" (CD "%LOCATION%" && CALL "%LOCATION%/debug.bat" && EXIT)
@echo "%CMD%"|findstr /I /R /C:"\<ECHO\ ON\ " >nul 2>&1
@if not errorlevel 1 (
	@SET "ECHOTOGGLE=true"
	CALL %CMD%
	ECHO OFF
	GOTO CONTINUE
) else (
	@echo "%CMD%"|findstr /I /R /C:"\<ECHO\ OFF\ " >nul 2>&1
	@if not errorlevel 1 (
		@SET "ECHOTOGGLE=false"
		CALL %CMD%
		ECHO OFF
		GOTO CONTINUE
	) else (
		@echo "%CMD%"|findstr /I /R /C:"\ ECHO\ ON\>" >nul 2>&1
		@if not errorlevel 1 (
			@SET "ECHOTOGGLE=true"
			CALL %CMD%
			ECHO OFF
			GOTO CONTINUE
		) else (
			@echo "%CMD%"|findstr /I /R /C:"\ ECHO\ OFF\>" >nul 2>&1
			@if not errorlevel 1 (
				@SET "ECHOTOGGLE=false"
				CALL %CMD%
				ECHO OFF
				GOTO CONTINUE
			) else (
				@echo "%CMD%"|findstr /I /R /C:"/?" >nul 2>&1
				@if not errorlevel 1 (
					%CMD%
				) else (
					@echo "%CMD%"|findstr /I /R /C:"-?" >nul 2>&1
					@if not errorlevel 1 (
						%CMD%
					) else (
						CALL %CMD%
					))))))
:CONTINUE
@SET "CDDIR=%CD%"
@GOTO CMD
:EXIT
@ENDLOCAL
@CD %LOCATION%
@ECHO OFF
@CSCRIPT //nologo "%~f0?.wsf" %*
@EXIT /B
-->
<job><script language="VBScript">
  msgbox "Thanks for using Entoli.", vbInformation+VbMsgBoxSETForeground, "Exiting Entoli - Good Bye and God Bless!"
</script></job>